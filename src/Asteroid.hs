module Asteroid where
import Laser
import System.Random

data Asteroid = Asteroid 
                {positionA :: (Int, Int), 
                 speedA :: Int, 
                 hitCounter :: Int, 
                 smart :: Bool, 
                 aliveA :: Bool} 


--update the astroid based on the laser it has been hit by
--hitAsteroid :: Asteroid -> Laser -> Asteroid
--hitAsteroid asteroid laser = if bad laser then asteroid else asteroid { alive = False}

--updates the position of the astroid based on its speed
moveAsteroid :: Asteroid -> Asteroid
moveAsteroid asteroid = asteroid { positionA = (x, y) }
    where x = fst (positionA asteroid) -- x stays the same
          yog = snd (positionA asteroid) --y origineel
          y = yog - speedA asteroid 

--checks if the asteroid is outside of bounds
asteroidInBounds :: Asteroid -> Bool
asteroidInBounds asteroid = snd (positionA asteroid) > (-200)


--return a new astroid with a random starting position
newAsteroid ::  Float -> Asteroid
newAsteroid eT= Asteroid (newNumber, 400) 2 0 False True
        where newNumber :: Int
              newNumber = fst (randomR (-200, 200) (mkStdGen (round eT)))
        

hitAsteroid :: Asteroid -> Laser -> Asteroid
hitAsteroid asteroid laser = if badL laser then asteroid else asteroid { aliveA = False}

--updates the gamestates asteroids
--moves the asteroids
--kills asteroids that are out of bounds
--removes dead asteroids
updateAsteroids :: [Asteroid] -> [Asteroid]
updateAsteroids asteroids = filter aliveA ( map updateOneAsteroid asteroids)
     

updateOneAsteroid :: Asteroid -> Asteroid
updateOneAsteroid asteroid = if asteroidInBounds newAsteroid then newAsteroid else asteroid {aliveA = False}
    where newAsteroid = moveAsteroid asteroid

    --do randomNumber <- randomIO
      -- let newNumber = abs randomNumber `mod` 10
       --return $ gstate { asteroids = (newAsteroid (randomNumber, 400) : asteroids gstate)}