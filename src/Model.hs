-- | This module contains the data types
--   which represent the state of the game
-- yuh
module Model where
import Spaceship
import Asteroid
import Laser
import Enemy

data InfoToShow = ShowNothing
                | ShowANumber Float
                | ShowAChar   Char
                | ShowObjects
                | ShowPaused

nO_SECS_BETWEEN_CYCLES :: Float
nO_SECS_BETWEEN_CYCLES = 5


periodMax :: (Float, Float)
periodMax = (5, 10)


data GameState = GameState {
                   infoToShow  :: InfoToShow
                 , elapsedTime :: Float
                 , periodTracker :: (Float, Float) --(AsteroidTime, EnemyTime)
                 , spaceship :: Spaceship
                 , asteroids :: [Asteroid]
                 , lasers :: [Laser]
                 , enemies :: [Enemy]
                 }

--gamestate starts with a spaceship in the center and no asteroids or enemies.
initialState :: GameState
initialState = GameState ShowObjects 0 (0, 0) (Spaceship (0, 0) True 12) [] [] []


