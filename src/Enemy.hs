module Enemy where

import Spaceship
import System.Random
data Enemy = Enemy 
            {positionE :: (Int, Int), 
            aliveE :: Bool, 
            speedE :: Int, 
            smartE :: Bool} 

--updates the gamestates enemies
updateEnemies :: Spaceship -> [Enemy] -> [Enemy]
updateEnemies spaceship@(Spaceship (xS, yS) alivS spS)  enemylist = map moveCloserToPlayer enemylist
  where
    --if
    moveCloserToPlayer :: Enemy -> Enemy
    moveCloserToPlayer enemy@(Enemy (xE, yE) alivE spE smrtE) | xE > xS = enemy { positionE = (xE - spE, yE)}
                                                              | xE < xS = enemy { positionE = (xE + spE, yE)}
                                                              | otherwise = enemy

--summons an enemy at the top of the screen at a random position
newEnemy ::  Float -> Enemy
newEnemy eT= (Enemy (newNumber, 155) True 1 True )
        where newNumber :: Int
              newNumber = fst (randomR ((-200), 200) (mkStdGen (1 + round eT)))