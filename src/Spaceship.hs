module Spaceship where


data Spaceship = Spaceship 
                {positionS :: (Int, Int), 
                 aliveS :: Bool, 
                 speedS :: Int} 
--edge of the screen
bound :: Int
bound = 200

--move the ship up down left or right based on the selected character and speed of the ship
--binds the position of the ship to the bounds of the screen
moveShip :: Spaceship -> Char -> Spaceship
moveShip ship@(Spaceship (x, y) _ speed) 'w' = ship { positionS = (x, (min bound (y + speed))) }
moveShip ship@(Spaceship (x, y) _ speed) 'a' = ship { positionS = ((max (- bound) (x - speed)), y) }
moveShip ship@(Spaceship (x, y) _ speed) 's' = ship { positionS = ((x, max (- bound) (y - speed))) }
moveShip ship@(Spaceship (x, y) _ speed) 'd' = ship { positionS = ((min bound (x + speed)), y) }
moveShip ship _ = ship

