module Laser where

data Laser = Laser 
            {positionL :: (Int, Int), 
            speedL :: Int,
            badL :: Bool, aliveL :: Bool}


--updates the gamestates lasers
updateLasers :: [Laser] -> [Laser]
updateLasers laserlist =
  let movedLasers = map moveLaser laserlist in
  let filteredLasers = filter laserInBounds movedLasers in
    filteredLasers

--moves a laser
moveLaser :: Laser -> Laser
moveLaser laser@(Laser (x, y) speed True _) = laser { positionL = (x, y - speed)}                     
moveLaser laser@(Laser (x, y) speed False _)= laser { positionL = (x, y + speed)}

--checks whether a laser is still in bounds                              
laserInBounds :: Laser -> Bool
laserInBounds laser@(Laser (x, y) speed True _) | y > (-200) = True
                                              | otherwise  = False
laserInBounds laser@(Laser (x, y) speed False _)| y < 200    = True
                                              | otherwise  = False

--shoots a laser at the tip of the ship and adds it to the game state
shootLaser :: (Int, Int) -> Laser
shootLaser (x, y)  = Laser (x + 8, y + 20) 3 False True