-- This module defines how to turn
--   the game state into a picture
module View where

import Graphics.Gloss
import Model
import Spaceship
import Laser
import Enemy

import Asteroid

--viewpure to IO
view :: GameState -> IO Picture
view = return . viewPure

--defines how to display the gamestate based on the current state
viewPure :: GameState -> Picture
viewPure gstate = case infoToShow gstate of
  ShowNothing   -> blank
  ShowANumber n -> translate (-200) 0 $ color green (text (show n))
  ShowAChar   c -> color green (text [c])
  ShowObjects   -> Pictures ((enemiesPicture gstate) : (shipPicture gstate) : (scorePicture gstate) : (asteroidPicture gstate) : (lasersPicture gstate) : [])
                  --draws the spaceship on the screen based on its location.
  ShowPaused    -> paused
--paused screen picture
paused :: Picture
paused = 
  let pausedMainText = translate (-200) 0 $ scale 0.6 0.6 $ color green (text "Paused :3") in
  let pausedBottomText = translate (-100) (-50) $ scale 0.2 0.2 $ color green (text "release to continue") in
  Pictures [pausedMainText, pausedBottomText]


--displays the player in their own location.
shipPicture :: GameState -> Picture
shipPicture gstate = 
  let x = (fst . positionS . spaceship) gstate in 
  let y = (snd . positionS . spaceship) gstate in 
  translate (fromIntegral x) (fromIntegral y) $ scale 0.3 0.3 $ color green (text "A")

--displays the score picture in the top right of the screen
scorePicture :: GameState -> Picture
scorePicture gstate = 
  let time = elapsedTime gstate in
  let score = round (100 * time) in
  let scoreText = show score in
  translate (-200) 175 $ scale 0.2 0.2 $ color green (text scoreText)

  -- i stole your code >:3
asteroidPicture :: GameState -> Picture
asteroidPicture gstate = Pictures (map drawSingleAstroid (asteroids gstate))

drawSingleAstroid :: Asteroid -> Picture
drawSingleAstroid asteroid = 
      let x = fst  (positionA  asteroid) in 
      let y = snd (positionA asteroid) in 
      translate (fromIntegral x) (fromIntegral y) $ scale 0.4 0.4 $  color yellow (text "O")


--combines all lasers in the gamestate into a single picture
lasersPicture :: GameState -> Picture
lasersPicture gstate =
  let pictures = map laserPicture (lasers gstate) in
  Pictures pictures

--colors enemy lasers red, own lasers green and turns them into a picture
laserPicture :: Laser -> Picture
laserPicture laser@(Laser (coord1, coord2) _ True _) = 
  let x = fromIntegral coord1 in
  let y = fromIntegral coord2 in
  translate x y $ scale 0.2 0.2 $ color red (text "l")
laserPicture laser@(Laser (coord1, coord2) _ False _) = 
  let x = fromIntegral coord1 in
  let y = fromIntegral coord2 in
  translate x y $ scale 0.2 0.2 $ color green (text "l")

--combines all enemies in the gamestate into a single picture
enemiesPicture :: GameState -> Picture
enemiesPicture gstate =
  let pictures = map enemyPicture (enemies gstate) in
    Pictures pictures

--draws the enemies
enemyPicture :: Enemy -> Picture
enemyPicture enemy@(Enemy (coord1, coord2) _ _ _) = 
  let x = fromIntegral coord1 in
  let y = fromIntegral coord2 in
    translate x y $ scale 0.3 0.2 $ color red (text "V")
