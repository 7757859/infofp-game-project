-- | This module defines how the state changes
--   in response to time and user input
module Controller where

import Model
import Spaceship
import Laser
import Enemy
import Asteroid

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import System.Random

-- | Handle one iteration of the game
step :: Float -> GameState -> IO GameState
--if paused, dont iterate
step secs gstate@(GameState ShowPaused _ _ _ _ _ _) = return gstate
step secs gstate
  = -- update all values
    do 
    return $ (addNewAsteroids.
              addNewEnemies .
              enemiesCollidePlayer .
              asteroidCollidePlayer .
              lasersHitEnemies .
              lasersHitAsteroids .
              lasersHitPlayer
              )
              (gstate { elapsedTime = elapsedTime gstate + secs,
              asteroids = updateAsteroids (asteroids gstate),
              lasers = updateLasers (lasers gstate),
              enemies = updateEnemies (spaceship gstate) (enemies gstate)})
  where 
    addNewAsteroids gstate = if asteroidTime > 5 then gstate { periodTracker = (0, enemyTime), asteroids = (newAsteroid (elapsedTime gstate)) : (asteroids gstate)} 
      else gstate {periodTracker = (asteroidTime, enemyTime)}
      where
        asteroidTime = secs + fst (periodTracker gstate)
        enemyTime = snd (periodTracker gstate)
    addNewEnemies gstate = if enemyTime > 12 then gstate { periodTracker = (asteroidTime, 0), enemies = (newEnemy (elapsedTime gstate)) : (enemies gstate)}
      else gstate { periodTracker = (asteroidTime, enemyTime)}
      where
        asteroidTime = fst (periodTracker gstate)
        enemyTime = secs + snd (periodTracker gstate)

-- | Handle user input
input :: Event -> GameState -> IO GameState
input e gstate = return (inputKey e gstate)

inputKey :: Event -> GameState -> GameState
--when pause button is pressed, switch between the game and the pause menu based on the current menu.
inputKey (EventKey (Char 'p') _ _ _) gstate@(GameState ShowObjects _ _ _ _ _ _) = gstate { infoToShow = ShowPaused }
inputKey (EventKey (Char 'p') _ _ _) gstate@(GameState ShowPaused _ _ _ _ _ _) = gstate { infoToShow = ShowObjects }
--if the game is paused, hold all other input
inputKey _ gstate@(GameState ShowPaused _ _ _ _ _ _) = gstate
--when the shoot button is pressed, shoot a laser
inputKey (EventKey (Char 'c') Down _ _) gstate = gstate{ lasers = shootLaser (positionS (spaceship gstate)) : lasers gstate}

inputKey (EventKey (Char 'k') Down _ _) gstate = gstate{ asteroids = newAsteroid (elapsedTime gstate) : asteroids gstate}


--for any other character, pass it to the moveship function
inputKey (EventKey (Char c) _ _ _) gstate
  = gstate { spaceship = moveShip (spaceship gstate) c}
  -- pass the character to the moveship function, which pattern matches the character

inputKey _ gstate = gstate


--try to put entity-specific functions inside their respective files
--by working with the specific types instead of using Gamestate for everything

--checks if a laser collides with a list of given enemies, updating both
lasersHitEnemies :: GameState -> GameState
lasersHitEnemies gstate@(GameState { lasers = laserlist, enemies = enemylist}) = gstate { lasers = newLasers, enemies = newEnemies}
  where
    update = lasersCollideEnemies laserlist enemylist
    newLasers = fst update
    newEnemies = snd update

--returns survivors of colliding lasers and enemies
lasersCollideEnemies :: [Laser] -> [Enemy] -> ([Laser], [Enemy])
lasersCollideEnemies laserlist enemylist =  lasersCollideEnemies' laserlist enemylist []

--returns laserlist as normal if there are no enemies
lasersCollideEnemies' (laser:laserlist) []        acc = (laser:laserlist, [])
--when all lasers have been handles, returns survivors of both lists
lasersCollideEnemies' []                enemylist acc = (filteredLasers, filteredEnemies)
  where
    filteredLasers = filter aliveL acc
    filteredEnemies = filter aliveE enemylist

--goes through the laserlist one at a time, storing the handles lasers in the accumulator
lasersCollideEnemies' (laser:laserlist) enemylist acc = lasersCollideEnemies' laserlist newEnemies (newLaser : acc)
  where
    updates = laserCollideEnemies laser enemylist
    newLaser = fst updates
    newEnemies = snd updates

--returns a laser and an updated list of enemies
laserCollideEnemies :: Laser -> [Enemy] -> (Laser, [Enemy])
laserCollideEnemies laser@(Laser { badL = True } ) enemies = (laser, enemies)
laserCollideEnemies laser []        = (laser, [])
laserCollideEnemies laser enemylist = (laserUpdate, enemylistUpdated)
  where
    enemylistUpdated = map (laserCollideEnemy laser) enemylist
    survives = map aliveE enemylist == map aliveE enemylistUpdated
    laserUpdate = laser { aliveL = survives}

--given a laser and enemy, it returns an enemy as dead if it collides with the laser. 
laserCollideEnemy :: Laser -> Enemy -> Enemy
laserCollideEnemy laser@(Laser { positionL = (xL, yL)}) enemy@(Enemy { positionE = (xE, yE)}) = enemy { aliveE = survives}
  where
    hitX = xL > xE && xL < xE + 16 --width of the enemy?
    hitY = yL + 16 > yE && yL < yE + 16 --height of the laser / height of the enemy?
    survives = not (hitX && hitY) --if not within bounds in both axes, enemy survives




--checks if a laser collides with a list of given asteroids, updating both
lasersHitAsteroids :: GameState -> GameState
lasersHitAsteroids gstate@(GameState { lasers = laserlist, asteroids = asteroidlist}) = gstate { lasers = newLasers, asteroids = newAsteroids}
  where
    update = lasersCollideAsteroids laserlist asteroidlist
    newLasers = fst update
    newAsteroids = snd update

--returns updated tuple with the lists
lasersCollideAsteroids :: [Laser] -> [Asteroid] -> ([Laser], [Asteroid])
lasersCollideAsteroids ls as = lasersCollideAsteroids' ls as []

--returns laserlist as normal if there are no asteroids
lasersCollideAsteroids' lasers            []        acc = (lasers        , [])
--when all lasers have been handled, returns survivors of both lists
lasersCollideAsteroids' []                asteroids acc = (filteredLasers, filteredAsteroids)
  where
    filteredLasers = filter aliveL acc
    filteredAsteroids = filter aliveA asteroids

--goes through the laserlist one at a time, storing the hadned lasers in the accumulator
lasersCollideAsteroids' (laser:laserlist) asteroids acc = lasersCollideAsteroids' laserlist newAsteroids (newLaser : acc)
  where
    updates      = laserCollideAsteroids laser asteroids
    newLaser     = fst updates
    newAsteroids = snd updates

--returns a laser and an updated list of enemies
laserCollideAsteroids :: Laser -> [Asteroid] -> (Laser, [Asteroid])
laserCollideAsteroids laser@(Laser { badL = True } ) asteroids = (laser, asteroids)
laserCollideAsteroids laser                          []        = (laser, [])
laserCollideAsteroids laser                          asteroids = (laserUpdate, asteroidsUpdated)
  where
    asteroidsUpdated = map (laserCollideAsteroid laser) asteroids
    survives         = map hitCounter asteroids == map hitCounter asteroidsUpdated
    laserUpdate      = laser { aliveL = survives}

--given a laser and enemy, it returns an enemy as dead if it collides with the laser. 
laserCollideAsteroid :: Laser -> Asteroid -> Asteroid
laserCollideAsteroid laser@(Laser { positionL = (xL, yL)}) asteroid@(Asteroid { positionA = (xA, yA), hitCounter = health, aliveA = lives}) = asteroid { hitCounter = newHealth, aliveA = survives}
  where
    hitX = xL > xA && xL < xA + 25 --width of the asteroid?
    hitY = yL + 20 > yA && yL < yA + 20 --height of the laser / height of the asteroid?
    hit = hitX && hitY 
    newHealth | hit       = health + 1
              | otherwise = health 
    survives = lives && (newHealth < 3) --if previously alive and hitcounter is below 3, asteroid survives

--checks if a laser collides with the player, updating both
lasersHitPlayer :: GameState -> GameState
lasersHitPlayer gstate = gstate

--checks if the player collides with an enemy, updating both
enemiesCollidePlayer :: GameState -> GameState
enemiesCollidePlayer gstate = gstate

--checks if the player collides with an asteroid, updating both
asteroidCollidePlayer :: GameState -> GameState
asteroidCollidePlayer gstate = gstate
